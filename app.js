var express = require('express');
var http = require('http');
var fs = require('fs');
var path = require('path');
var Jimp = require('jimp');
var kmeans = require('node-kmeans');
var app = express();

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function (req,res) {
	console.log('Downloading input');
	var file = fs.createWriteStream("public/input-file.jpg");
	var request = http.get('http://24.media.tumblr.com/tumblr_lfp3qax6Lm1qfmtrbo1_1280.jpg', function(response) {
		response.pipe(file);
		file.on('finish', function() {
			file.close(processImg(res));
		});	
	}).on('error', function(err) { 
		console.log(err);
		fs.unlink(dest);
		if (cb) cb(err.message);
	});
});

var processImg = function(res){
	console.log('Input downloaded');
	Jimp.read("public/input-file.jpg", function (err1, input) {
		console.log('Input readed');
		Jimp.read("public/mask.png", function (err, mask) {
			console.log('Mask readed');
		    var output = input.clone();
		    var width = input.bitmap.width;
		    var height = input.bitmap.height;
		    var vectors = new Array();
		    var pixelId = 0;
		    var idsArray = new Array();
			for (i = 0; i < height; i++){
		    	for (j = 0; j < width; j++){
		    		console.log('Input pixel: ' + j + ', ' + i);
		    		var maskColor = Jimp.intToRGBA(mask.getPixelColor(j,i));
		    		var color = Jimp.intToRGBA(input.getPixelColor(j,i));
		    		var coordenates = [color.r, color.g, color.b];
		    		if(maskColor.r===255 && maskColor.g===255 && maskColor.b===255){
		    			vectors[pixelId] = coordenates;
		    			idsArray[pixelId] = [j,i];
	    				pixelId++;
		    		}
		    	}
		    }
		    console.log("Data completed");
		    kmeans.clusterize(vectors, {k: 30}, (err,clusters) => {
				if (err) {
					console.error(err);
				} else { 
					console.log("Kmeans finished");
					clusters.forEach(function(cluster, index){
	    				var ids = cluster.clusterInd;
	    				var newColor = cluster.centroid;
    					var newR = newColor[0];
    					var newG = newColor[1];
    					var newB = newColor[2];
    					var gray = (newR + newG + newB)/3;
	    				ids.forEach(function(id, index){
	    					var pixelPos = idsArray[id];
	    					console.log("New Color for: " + pixelPos[0] + ', ' + pixelPos[1]);
		    				output.setPixelColor(Jimp.rgbaToInt(gray,gray,gray,255), pixelPos[0], pixelPos[1]);
	    				});
	    			});
				    output.write("public/output-file.jpg", showImage(res));
				}
			});
		});
	}); 
}

var showImage = function(res){
	console.log("Segmentation finished");
	res.send('<img src="output-file.jpg">');
}

app.listen(3000, function() {
	console.log('Example app listening on port 3000!')
});
