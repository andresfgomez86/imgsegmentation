To achieve the image segmenteation as desired, I used the following steps:

1) Download the input image and save a copy into the project .
2) Iterate over the pixels of the downloaded file and the mask file.
3) For each input pixel that corresponds to a white pixel in the mask file, collect the RGB values into an array using a consecutive id.
3) At the same time, create an Array to save the XY coordinates of each pixel's RGB saved in the previous Array.
4) Using the node-kmeans module (Kmeans method), segmentate the RGB data collected into 30 clusters. 
5) Itereate over the clusters generated and get the RGB values of each centroid.
6) Get the grayscale values of the centroid making an average of the RGB values.
7) Asignate the centroid's grayscale value to the pixels of its cluster.
8) Save the output file and display it on the browser.